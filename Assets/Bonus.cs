﻿using UnityEngine;
using System.Collections;

public class Bonus : MonoBehaviour {

	public int bonusType {
		get;
		set;
	}

	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody>().AddForce(Vector3.back * 4, ForceMode.VelocityChange);
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnCollisionEnter( Collision collision )
	{
		if (collision.gameObject.layer == LayerMask.NameToLayer("Board"))
		{
			CS_GameDirector.m_instance.ApplyBonus(bonusType);

			Destroy(gameObject);
		}

		if (collision.gameObject.layer == LayerMask.NameToLayer("Fail"))
		{		
			Destroy(gameObject);
		}
	}
}
