﻿using UnityEngine;
using System.Collections;

public class powerup_extender : MonoBehaviour {

	public bool powerupExtender;

	// Use this for initialization
	void Start () {
		powerupExtender = false;
	}
	
	// Update is called once per frame
	void Update () {
		powerupExtender = CS_GameDirector.m_instance.BonusExtender;
		if (powerupExtender == false){
			gameObject.SetActive(false);
		}else{
			gameObject.SetActive(true);
		}
	}
}
