﻿using UnityEngine;
using System.Collections;

public class CS_Ball : CS_GameEntity
{
	public float                               m_Speed = 10; 
	//public AudioSource                         m_BounceSound = null; // zvukovy efekt pri narazu kulicky
	
	bool                                       m_Captured; // udrzuje informaci o tom, jestli je kulicka prilepena k desce nebo se pohybuje

	
	void Awake()
	{
		m_Captured = false;
	}
	
	void OnCollisionEnter( Collision collInfo )
	{ // detekce narazu kulicky
		if (collInfo.gameObject.layer == LayerMask.NameToLayer("Fail"))
			GameDirectorInstance.FailBall(); // pokud narazila do objektu s layerem Fail
		//else
			//if (m_BounceSound != null)
			//	m_BounceSound.Play();
	}
	
	void FixedUpdate()
	{
			// fyzikalni odrazy obcas zpusobi, ze se pohyb kulicky zmeni na temer kolmy nebo svisly
			// v tomto pripade dojde k umelemu vychyleni kulicky, at leti vice zesikma
		Vector3 velocity = GetComponent<Rigidbody>().velocity;
		if (velocity.sqrMagnitude < 1) // uplatnuje se ud urcite rychlosti
			return;
		
		if (Mathf.Abs(velocity.x) < 0.4f)
			GetComponent<Rigidbody>().AddForce(new Vector3(Time.deltaTime * 5000 * -Mathf.Sign(transform.position.x), 0, 0), ForceMode.Acceleration);
		if (Mathf.Abs(velocity.z) < 0.8f)
			GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, Time.deltaTime * 5000 * -Mathf.Sign(transform.position.z)), ForceMode.Acceleration);
	}
	
	public void CaptureBall( GameObject board )
	{ // prilepi kulicku na hraci desku
		transform.position = board.transform.position + Vector3.forward;
		transform.parent = board.transform;
		GetComponent<Rigidbody>().isKinematic = true;
		m_Captured = true;
	}
	
	public void ReleaseBall()
	{ // uvolni kulicku a udeli ji pocatecni impuls
		transform.parent = null;
		GetComponent<Rigidbody>().isKinematic = false;
		m_Captured = false;
		
		GetComponent<Rigidbody>().AddForce(new Vector3(-0.4f * Mathf.Sign(transform.position.x), 0, 1) * 10, ForceMode.VelocityChange);
	}
	
	public bool Captured
	{
		get { return m_Captured; }
	}
	
	public void SpeedUp( float ratio )
	{
		if (!GetComponent<Rigidbody>().isKinematic) // nepracuju s rychlosti, kdyz je prilepeny k palce
		{
			Vector3 velocity = GetComponent<Rigidbody>().velocity;
			float speed = velocity.magnitude * ratio;
			if (speed > 1)
			{ // jen od urcite rychlosti, neaplikovcat na stojici kulicku
				speed = Mathf.Clamp(speed, 5, 20);
				velocity.Normalize();
				GetComponent<Rigidbody>().velocity = velocity * speed;
			}
		}
	}
}
