﻿using UnityEngine;
using System.Collections;

public class CS_Board : CS_GameEntity
{
	public float                               m_MovementRangeLimit = 6; // limitni hodnota pro pohyb palky
	
	float                                      m_LastMousePosX; // pozice mysi z minuleho snimku


	void Start()
	{
		m_LastMousePosX = Input.mousePosition.x;
	}
	
	void Update()
	{
			// rozdil z minuleho snimku
		float deltaX = (Input.mousePosition.x - m_LastMousePosX) * Time.timeScale; // nasobeni Time.timeScale zpusobi zpomaleni nebo zastaveni pohybu desky spolu s fyzikou
		m_LastMousePosX = Input.mousePosition.x;
		
			// orezani nove pozice do limitu a nastaveni game objektu
		Vector3 pos = transform.position;
		pos.x += deltaX * 0.05f;
		if (pos.x < -m_MovementRangeLimit)
			pos.x = -m_MovementRangeLimit;
		else if (pos.x > m_MovementRangeLimit)
			pos.x = m_MovementRangeLimit;
		transform.position = pos;
	}

	void OnCollisionEnter( Collision collision )
	{
		foreach (ContactPoint contact in collision.contacts) {
			if(contact.otherCollider.name == "Ball")
				CS_GameDirector.m_instance.decreaseBonusCounter();
		}
	}

}
