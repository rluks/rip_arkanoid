﻿using UnityEngine;
using System.Collections;

public class CS_Coffin : MonoBehaviour {

	public int hitpoints;
	public GameObject BloodFountain;
	public GameObject ZOMBIE;
	public int powerUp;

	public AudioSource m_BleedingSound = null;

	// Use this for initialization
	void Start () {
		powerUp = Random.Range(0, 3) % 3;
		//TODO diff color
		hitpoints = Storage.instance.CoffinHP;//CS_GameDirector.m_instance.CoffinHP;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter( Collision collision ) {
		foreach (ContactPoint contact in collision.contacts) {
			GameObject bloodfountain = Instantiate( BloodFountain, contact.point, Quaternion.LookRotation(-contact.normal)) as GameObject;
			if(bloodfountain != null)
				bloodfountain.transform.parent = transform;
		}

		hitpoints--;

		CS_GameDirector.m_instance.AddScore (1);

		if (hitpoints >= 1) {//rakev bude krvacet

			CS_GameDirector.m_instance.AddScore (5);

			if (m_BleedingSound != null)
				m_BleedingSound.Play();
		}
		if(hitpoints <= 0)
		{
			CS_GameDirector.m_instance.AddScore (10);

			if(powerUp >= 1){
				GameObject bonusGO = Instantiate(ZOMBIE, transform.position, Quaternion.identity) as GameObject;
				Bonus bonus = bonusGO.GetComponent<Bonus>();
				if(bonus == null){
					Debug.LogError("Nemam Bonus script");}
				else{
					bonus.bonusType = powerUp;
				}
			}

			Destroy(gameObject);
		}
	}
}
