﻿using UnityEngine;
using System.Collections;

public class Storage : MonoBehaviour {

	public static Storage instance = null;

	public int                                        m_ActLevel; // aktualni level hry
	public int                                        m_Score; // hracovo score

	public int CoffinHP;

	// Use this for initialization
	void Awake () {

		if(instance != null)
			Destroy(gameObject);
		else
			instance = this;
		
		DontDestroyOnLoad(gameObject);
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
