﻿using UnityEngine;
using System.Collections;

public class CS_Brick : CS_GameEntity
{
	int                                        m_Level; // aktualni level cihly

	
	void Awake()
	{
		Level = 1;
	}
	
	void Update()
	{
			// pokud propadne pod stanovenou hreanici, zrusim cely objekt
		if (transform.position.y < -5)
			Destroy(gameObject);
	}
	
	void OnCollisionEnter( Collision collInfo )
	{ // detekce na zasah kulicky
		if (GameDirectorInstance != null)
			GameDirectorInstance.AddScore(m_Level);
		
		if (m_Level > 1)
		{ // pokud ma level > 1, tak nezmizi, ale jen snizi level a zmeni barvu
			Level--;
			return;
		}

		/*
		if (GameDirectorInstance != null)
			GameDirectorInstance.BrickHit(this);
		*/
			// pokud ma cihla zmizet, zapne se ji fyzika, vypne kolize, at neovlivnuje zbytek hry a dostane impuls pro rotaci a let nahoru
		gameObject.GetComponent<Collider>().enabled = false;
		GetComponent<Rigidbody>().isKinematic = false;
		
		GetComponent<Rigidbody>().AddForce(Vector3.up * 15, ForceMode.VelocityChange);
		
		float torgueRange = 5;
		Vector3 torqueVector = new Vector3(Random.Range(-torgueRange, torgueRange), Random.Range(-torgueRange, torgueRange), Random.Range(-torgueRange, torgueRange));
		GetComponent<Rigidbody>().AddTorque(torqueVector, ForceMode.VelocityChange);
	}

	public int Level
	{
		get { return m_Level; }
		set
		{
			m_Level = value;
			
			if (GetComponent<Renderer>().material != null)
				GetComponent<Renderer>().material.color = GetColorByLevel(m_Level);
		}
	}
	
	
	Color GetColorByLevel( int level )
	{ // definuje barvu cihly podle levelu
		Color[] colors = { new Color(0, 1, 0, 1),
						   new Color(0.8f, 0.8f, 0, 1),
						   new Color(1, 0, 0, 1),
						   new Color(0.5f, 0, 0.5f, 1),
						   new Color(0.4f, 0.4f, 1, 1),
						   new Color(1, 0.6f, 0.2f, 1),
						   new Color(0, 0.5f, 0.5f, 1),
						   new Color(0.3f, 0.3f, 0.3f, 1),
						   new Color(1, 1, 1, 1),
		};
		
		return colors[(level - 1) % colors.Length];
	}
}
