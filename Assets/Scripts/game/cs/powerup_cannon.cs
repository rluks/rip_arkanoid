﻿using UnityEngine;
using System.Collections;

public class powerup_cannon : MonoBehaviour {
	public Rigidbody projectile;
	public bool powerupCannon;
	int ammo = 5 ;
	// Use this for initialization
	void Start () {
	
		powerupCannon = false;
	}
	
	// Update is called once per frame
	void Update () {
		powerupCannon = CS_GameDirector.m_instance.BonusCannon;
		if (powerupCannon == false){
			gameObject.SetActive(false);
			ammo = 5;
		}else{
			gameObject.SetActive(true);

			if (ammo > 0) {
				
				if (Input.GetButtonDown ("Fire1")) {
					ammo--;
					Rigidbody clone;
					clone = Instantiate (projectile, transform.position, transform.rotation) as Rigidbody;
					clone.velocity = transform.TransformDirection (Vector3.forward * 10);
				}
			}
		}
	}
}



