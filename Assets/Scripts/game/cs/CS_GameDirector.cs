﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CS_GameDirector : MonoBehaviour
{
	public CS_Board                            m_Board = null; // objekt Board
	public GameObject	board_extender = null;
	public GameObject	skull_cannon = null;
	public CS_Ball                             m_Ball = null; // objekt Ball

	
	int                                        m_ActLevel; // aktualni level hry
	int                                        m_Score; // hracovo score

	public int CoffinHP;
	public bool BonusExtender{ set; get; }
	public bool BonusCannon{ set; get; }
	public static CS_GameDirector  			   m_instance = null ;
	

	int                                        m_FailBalls; // pocet propadnutych kulicek
	string                                     m_Message; // zprava, ktera se ma vypisovat uzivateli
	List<CS_Brick>                             m_Bricks; // seznam vygenerovanych cihel
	List<CS_Bonus>                             m_Bonuses; // seznam vygenerovanych bonusu

	public AudioSource m_BonusExtenderSound = null;
	public AudioSource m_BonusCannonSound = null;

	int bonusCounter;
	
	void Awake()
	{
		if ((m_Ball == null) || (m_Board == null))
		{ // kontrtola, pokud je nastaveno vsechno spravne
			Debug.LogError("GameDirector nema vyplnene polozky Ball a/nebo Board a/nebo BrickPrefab a/nebo BonusPrefab!");
			gameObject.SetActive(false);
		}
		else
		{ // nastaveni instance GameDirector pro zpetna volani
			m_Ball.GameDirectorInstance = this;
			m_Board.GameDirectorInstance = this;

			m_FailBalls = 0;
			m_Message = string.Empty;
			m_Bricks = new List<CS_Brick>();
			m_Bonuses = new List<CS_Bonus>();
			m_instance = this;
			bonusCounter = 0;
			
			BonusExtender = false;
			BonusCannon = false;

			CoffinHP = 1;
		}
	}
	
	void Start()
	{
		Cursor.visible = false;

		m_ActLevel = Storage.instance.m_ActLevel;
		m_Score = Storage.instance.m_Score;
		CoffinHP = Storage.instance.CoffinHP;


		m_Ball.CaptureBall(m_Board.gameObject);
		SetMessage("Use mouse to board movemet.\nPress left mouse button to release ball.");
	}
	
	
	void Update()
	{
		if (Input.GetKeyDown ("escape")) {
			BonusExtender = false;
			BonusCannon = false;

			Storage.instance.m_ActLevel = 0;
			Storage.instance.m_Score = 0;
			Storage.instance.CoffinHP = 1;

			Application.LoadLevel(0);
		}

		if (m_Ball.Captured && (Input.GetMouseButtonDown(0)))
		{
			m_Ball.ReleaseBall();
			ClearMessage();
		}

		if(bonusCounter <= 0){
			BonusExtender = false;
			BonusCannon = false;
		}

		if(GameObject.FindWithTag("Coffin") == null){

			m_ActLevel++;
			int nextLevel = m_ActLevel % 3 + 1;

			CoffinHP++;


			BonusExtender = false;
			BonusCannon = false;

			
			Storage.instance.m_ActLevel = m_ActLevel;
			Storage.instance.m_Score = m_Score;
			Storage.instance.CoffinHP = CoffinHP;

			Application.LoadLevel(nextLevel);
		}
	}

	public void FailBall()
	{ // vola se, pokud kulicka propadne
		m_Ball.CaptureBall(m_Board.gameObject);
		m_FailBalls++;

		AddScore(-1000);
		DestroyAllBonuses();
		
		SetMessage("!!! FAIL BALL !!!\nPress left mouse button to release ball.");
		StartCoroutine("FlashBackground");
	}

	/*
	public void BrickHit( CS_Brick brick )
	{
		Debug.Log ("BRICKHIT ");
		m_Bricks.Remove(brick);
		if (m_Bricks.Count < 1)
		{ // vitezstvi
			m_Score += m_ActLevel * 10;
			m_ActLevel++;
			//CreateBricks(m_ActLevel);
			m_Ball.CaptureBall(m_Board.gameObject);
			DestroyAllBonuses();
			
			SetMessage("Level complete!\nPress left mouse button to release ball.");
		}
		else
			CreateBonus(brick.transform.position);
	}
	*/
	
	public void AddScore( int points )
	{
		m_Score += points;
	}
	
	public void SetMessage( string message )
	{
		m_Message = message;
	}
	
	public void ClearMessage()
	{
		m_Message = string.Empty;
	}
	
	public void ApplyBonus( int bonus )
	{
		switch (bonus)
		{
		case 1://extender
			Debug.Log("GD-ApplyBonus extender");
			m_Score += 50;
			BonusExtender = true;
			if (m_BonusExtenderSound != null)
				m_BonusExtenderSound.Play();
			board_extender.SetActive(true);
			restartBonusCounter();
			break;
		case 2://canon
			Debug.Log("GD-ApplyBonus cannon");
			BonusCannon = true;
			if (m_BonusCannonSound != null)
				m_BonusCannonSound.Play();
			skull_cannon.SetActive(true);
			restartBonusCounter();
			m_Score += 50;
			break;
		case 3:
			m_Score += 50;
			m_Ball.SpeedUp(0.5f);
			break;
		case 4:
			m_Score += 50;
			break;
		default : Debug.LogError("Neznamy bonus");
			break;
		}
	}

	/*
	void CreateBonus( Vector3 pos )
	{ // vytvori novou instanci bonusu
		//if (Random.Range(0, 100) > m_BonusPercentage)
		//	return;
		
		/*GameObject bonus = (GameObject)Instantiate(m_BonusPrefab);
		if (bonus != null)
		{
			bonus.transform.position = pos;
			
			CS_Bonus bonusScript = bonus.GetComponent<CS_Bonus>();
			if (bonusScript != null)
				bonusScript.GameDirectorInstance = this;
			
			m_Bonuses.Add(bonusScript);
		}
	}
	*/
	
	public void DestroyBonus( CS_Bonus bonus )
	{
		m_Bonuses.Remove(bonus);
		Destroy(bonus.gameObject);
	}
	
	void DestroyAllBonuses()
	{
		for (int i=0 ; i<m_Bonuses.Count ; i++)
			Destroy(m_Bonuses[i].gameObject);
		m_Bonuses.Clear();
	}
	
	void DecBricksLevel()
	{
		for (int i=0 ; i<m_Bricks.Count ; i++)
			m_Bricks[i].Level = Mathf.Max(m_Bricks[i].Level - 1, 1);
	}
	
	void OnGUI()
	{
		string status = "Level: " + (m_ActLevel+1).ToString();
		status += "\nScore: " + m_Score;
		status += "\nBricks left: " + m_Bricks.Count;
		status += "\nFail balls: " + m_FailBalls;
		GUI.Box(new Rect(10, 10, 160, 70), status);
		
		if (!string.IsNullOrEmpty(m_Message))
		{
			int centerX = Screen.width / 2;
			int centerY = Screen.height / 2;
			int boxHalfSizeX = Screen.width / 3;
			GUI.Box(new Rect(centerX - boxHalfSizeX, centerY, boxHalfSizeX * 2, 40), m_Message);
		}

	}
	
	IEnumerator FlashBackground()
	{
		Camera actCam = Camera.main;
		if (actCam == null)
			yield break;
		
		Color origColor = actCam.backgroundColor;
		Color warningColor = new Color(1, 0, 0, 1);
		
		for (int i=0 ; i < 5 ; i++)
		{
			actCam.backgroundColor = warningColor;
			yield return new WaitForSeconds(0.1f);
			actCam.backgroundColor = origColor;
			yield return new WaitForSeconds(0.1f);
		}
		
		yield break;
	}

	public void decreaseBonusCounter(){
		bonusCounter--;
	}

	void restartBonusCounter(){
		bonusCounter = 3;
	}
}
